@extends('administrator.admin-base')

@section('admin-contents')

<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded p-4">
        <h2>Members' List</h2>
        <div class="table-responsive mt-3">
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Number</th>
                    <th>Address</th>
                    <th>Package</th>
                    <th>Account</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($members as $key=>$member)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$member->name}}</td>
                        <td>{{$member->mobile}}</td>
                        <td>{{$member->address}}</td>
                        <td>{{$member->package}}</td>
                        <td>{{$member->account_type}}</td>
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-secondary btn-sm dropdown-toggle members-upgrade p-0" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <iconify-icon icon="tabler:dots" style="font-size:2em"></iconify-icon>
                                </button>
                                <ul class="dropdown-menu px-3 py-1">
                                    <div><a href="#">Edit</a></div>
                                    <div><a href="/admin/upgrade-member/{{$member->id}}">Upgrade to BCO</a></div>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $members->links() }}
        </div>
    </div>
</div>

@endsection