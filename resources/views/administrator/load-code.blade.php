@extends('administrator.admin-base')

@section('admin-contents')

<div class="container-fluid pt-4 px-4">
    <div class="row g-4">
        <div class="col-sm-6 col-lg-4">
            <div class="bg-light rounded d-flex align-items-center justify-content-between p-4">
                <div class="ms-3">
                    <p class="mb-2">Available Codes</p>
                    <h6 class="mb-0">0</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-4">
            <div class="bg-light rounded d-flex align-items-center justify-content-between p-4">
                <div class="ms-3">
                    <p class="mb-2">Purchased Codes</p>
                    <h6 class="mb-0">0</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row px-3">
        <div class="col-sm-12 col-md-6 col-lg-8">
            <button class="btn btn-primary my-5 w-25" data-bs-toggle="modal" data-bs-target="#loadCode">Load Codes to Member</button>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Transaction ID</th>
                    <th>Sold To</th>
                    <th>Qty</th>
                    <th>Package</th>
                    <th>Total</th>
                    <th>Date</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($pin_requests as $key => $pin_request)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$pin_request->trans_num}}</td>
                        <td>{{$pin_request->name}}</td>
                        <td>{{$pin_request->qty}}</td>
                        <td>{{$pin_request->package}}</td>
                        <td>{{$pin_request->total}}</td>
                        <td>{{$pin_request->created_at->format('Y-m-d')}}</td>
                        <td><a href="{{route('generate-code', ['id' => $pin_request->id])}}" class="btn btn-primary py-1 px-2 fs-7">Load</a></td>
                    </tr>
                   @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Load Code Modal -->
<div class="modal fade" id="loadCode" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Load Code to Members</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="/admin/load-code-to-member" method="POST">
        @csrf
        <div class="modal-body">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Search for Member</label>
                        <input type="text" id="autocomplete" class="form-control" placeholder="Search..." autocomplete="off" required>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Member User ID</label>
                        <input type="number" id="selected-id" name="user_id" class="form-control" readonly>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Select Package</label>
                        <select name="package" id="package" class="form-select" required>
                            <option value="" disabled selected></option>
                            <option value="silver">Silver</option>
                            <option value="gold">Gold</option>
                            <option value="diamond">Diamond</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Qty</label>
                        <input type="number" class="form-control" min="0" step="1" name="qty" id="qty" required> 
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Generate Code</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- resources/views/your-view.blade.php -->
<script type="text/javascript">
    var route = "{{ route('autocomplete-name') }}";
    $('#autocomplete').typeahead({
        source: function (query, process) {
            return $.get(route, {
                query: query
            }, function (data) {
                return process(data);
            });
        },
        updater: function (item) {
            var selectedUser = item;
            $('#selected-id').val(selectedUser.id);

            return selectedUser.name;
    }
    });
</script>

<script src="{{asset('js/script.js')}}"></script>
@endsection()