@extends('administrator.admin-base')

@section('admin-contents')

<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded p-4">
        <h2>Members' Encashments</h2>

        <ul class="nav nav-tabs mt-3" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="pending-tab" data-bs-toggle="tab" data-bs-target="#pending-tab-pane" type="button" role="tab" aria-controls="pending-tab-pane" aria-selected="true">Pending</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="released-tab" data-bs-toggle="tab" data-bs-target="#released-tab-pane" type="button" role="tab" aria-controls="released-tab-pane" aria-selected="false">Released</button>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="pending-tab-pane" role="tabpanel" aria-labelledby="pending-tab" tabindex="0">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <th>#</th>
                            <th>Account</th>
                            <th>Amount</th>
                            <th>Details</th>
                            <th>Mobile</th>
                            <th>Date</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($pendingEncashments as $key=>$p_encashment)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$p_encashment->user->name}}</td>
                                <td>&#8369; {{number_format($p_encashment->amount,2)}}</td>
                                <td>{{$p_encashment->details}}</td>
                                <td>{{$p_encashment->user->mobile}}</td>
                                <td>{{$p_encashment->created_at->format('m-d-y')}}</td>
                                <td>
                                    <a href="/admin/approve-encashment/{{$p_encashment->id}}" class="btn btn-primary" style="font-size: 13px; padding: 1px 10px;">Approve</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $pendingEncashments->links() }}
            </div>
            <div class="tab-pane fade" id="released-tab-pane" role="tabpanel" aria-labelledby="released-tab" tabindex="0">
            <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <th>#</th>
                            <th>Account</th>
                            <th>Amount</th>
                            <th>Details</th>
                            <th>Mobile</th>
                            <th>Date</th>
                        </thead>
                        <tbody>
                            @foreach($releasedEncashments as $key=>$r_encashment)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$r_encashment->user->name}}</td>
                                <td>&#8369; {{number_format($r_encashment->amount,2)}}</td>
                                <td>{{$r_encashment->details}}</td>
                                <td>{{$r_encashment->user->mobile}}</td>
                                <td>{{$r_encashment->created_at->format('m-d-y')}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $releasedEncashments->links() }}
            </div>
        </div>
    </div>
</div>


@endsection