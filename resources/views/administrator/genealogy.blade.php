@extends('administrator.admin-base')

@section('admin-contents')

<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded p-4">
        <h2>Genealogy</h2>
        <div class="w-25">
            <form action="" method="GET">
                <label for="" class="form-label">Search for Member's Genealogy</label>
                <input type="text" id="autocomplete" class="form-control" placeholder="Search..." autocomplete="off" required>
                <input type="hidden" id="selected-id">
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var route = "{{ route('autocomplete') }}";
    $('#autocomplete').typeahead({
        source: function (query, process) {
            return $.get(route, {
                query: query
            }, function (data) {
                return process(data);
            });
        },
        updater: function (item) {
            var selectedUser = item;
            $('#selected-id').val(selectedUser.id);

            return selectedUser.name;
    }
    });
</script>

<script src="{{asset('js/admin-genealogy.js')}}"></script>

@endsection