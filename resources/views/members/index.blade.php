@extends('base')

@section('contents')

<div class="container-fluid pt-4 px-4">
    <div class="row g-4 mb-3">
        <div class="col-sm-6 col-xl-3">
            <div class="bg-light rounded d-flex align-items-center justify-content-between py-3 px-2">
                <div class="ms-3">
                    <p class="mb-2">Total Income</p>
                    <h6 class="mb-0">&#8369; {{number_format(@$total_income,2)}}</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="bg-light rounded d-flex align-items-center justify-content-between py-3 px-2">
                <div class="ms-3">
                    <p class="mb-2">Total Pairing</p>
                    <h6 class="mb-0">&#8369; {{number_format(@$total_pairing,2)}}</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="bg-light rounded d-flex align-items-center justify-content-between py-3 px-2">
                <div class="ms-3">
                    <p class="mb-2">Total Referral</p>
                    <h6 class="mb-0">&#8369; {{number_format(@$total_referral,2)}}</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="bg-light rounded d-flex align-items-center justify-content-between py-3 px-2">
                <div class="ms-3">
                    <p class="mb-2">Total Encashments</p>
                    <h6 class="mb-0">&#8369; {{@$total_encashments,2}}</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row g-4">
        <div class="col-sm-6 col-xl-3">
            <div class="bg-light rounded d-flex align-items-center justify-content-between py-3 px-2">
                <div class="ms-3">
                    <p class="mb-2">Dream Points</p>
                    <h6 class="mb-0">{{number_format(@$income->dream_points,2)}}</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="bg-light rounded d-flex align-items-center justify-content-between py-3 px-2">
                <div class="ms-3">
                    <p class="mb-2">Mutual Fund</p>
                    <h6 class="mb-0">&#8369; </h6>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="bg-light rounded d-flex align-items-center justify-content-between py-3 px-2">
                <div class="ms-3">
                    <p class="mb-2">Current Profit Sharing</p>
                    <h6 class="mb-0">&#8369; </h6>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="bg-light rounded d-flex align-items-center justify-content-between py-3 px-2">
                <div class="ms-3">
                    <p class="mb-2">Total Profit Sharing</p>
                    <h6 class="mb-0">&#8369; </h6>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt-4 px-4 history-dashboard">
    <div class="bg-light rounded p-4">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Transaction</th>
                    <th>Amount / Points</th>
                    <th>Date</th>
                </thead>
                <tbody>
                    @foreach($histories as $key=>$history)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$history->transactions}}</td>
                        <td>{{number_format($history->amount_points,2)}}</td>
                        <td>{{$history->created_at->format('m-d-y')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$histories->links()}}
        </div>
    </div>
</div>

@endsection