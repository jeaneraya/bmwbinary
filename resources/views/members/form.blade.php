@extends('base')

@section('contents')

<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded p-4">
        <div class="row g-4">
            <div class="col-8">
                <h2>Member Registration</h2>
                <form action="/member/registration" method="POST" class="mt-4">
                @csrf
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">First Name</label>
                        <div class="col-sm-10">
                            <input type="hidden" name="transaction" value="registration">
                            <input type="text" class="form-control" id="first_name" name="first_name">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Last Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="last_name" name="last_name">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Birthday</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="birthdate" name="birthdate">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="address" name="address">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Mobile No.</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="mobile" name="mobile" maxlength="11" step="1" min="0">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Sponsor</label>
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" id="sponsor" name="sponsor" value="{{auth()->user()->id}}" readonly>
                            <input type="text" class="form-control" value="{{auth()->user()->name}}" readonly>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Placement</label>
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" id="placement" name="placement" value="{{$datas['placement']}}">
                            <input type="text" class="form-control" value="{{$placement}}" readonly>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Side</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="side" name="side" value="{{$datas['side']}}" readonly>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Select Pin</label>
                        <div class="col-sm-10">
                            <select name="pins" id="pins" class="form-select">
                                <option value="" disabled selected></option>
                                @foreach ($pins as $pin)
                                    @if($pin->load_to !== 'bco')
                                    <option value="{{@$pin->id}}">{{@$pin->pin_key}} {{@$pin->pin_code}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="username" name="username">
                        </div>
                    </div>
                    <div class="row mb-5">
                        <label for="" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                    </div>
                    <div class="row mb-3 d-flex justify-content-end">
                        <button class="btn btn-primary w-25" type="submit">Submit Registration</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@if(session('failed'))
<script>
    Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: '{{ session('failed') }}',
            confirmButtonText: 'OK'
        });
</script>
@endif

@endsection