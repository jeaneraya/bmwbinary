@extends('base')

@section('contents')

<div class="container-fluid pt-4 px-4">
    <div class="row g-4">
        <div class="col-sm-6 col-lg-4">
            <div class="bg-light rounded d-flex align-items-center justify-content-between p-4">
                <div class="ms-3">
                    <p class="mb-2">Purchased Codes</p>
                    <h6 class="mb-0">{{$purchased_code}}</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-8">
            <div class="bg-light rounded d-flex align-items-center justify-content-between p-4">
                <div class="ms-3">
                    <p class="mb-2">Available Codes</p>
                    <h6 class="mb-0">
                        <span class="text-success">Silver: {{@$silver}}</span>
                        <span class="text-warning mx-4">Gold: {{@$gold}}</span>
                        <span class="text-primary">Diamond: {{@$diamond}}</span>
                    </h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row px-3">
        <div class="col-sm-12 col-md-6 col-lg-8">
            <button class="btn btn-primary my-5" data-bs-toggle="modal" data-bs-target="#loadCode">Load Codes to Member</button>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Sold To</th>
                    <th>Qty</th>
                    <th>Package</th>
                    <th>Total</th>
                    <th>Date</th>
                </thead>
                <tbody>
                    @foreach(@$bco_load_histories as $key => $blh)
                    <tr>
                        <td>{{@$key + 1}}</td>
                        <td>{{@$blh->user->name}}</td>
                        <td>{{@$blh->qty}}</td>
                        <td>{{@$blh->package}}</td>
                        <td>&#8369; {{number_format(@$blh->amount,2)}}</td>
                        <td>{{@$blh->created_at->format('Y-m-d')}}</td>
                    </tr>
                   @endforeach
                </tbody>
            </table>
            {{ $bco_load_histories->links() }}
        </div>
    </div>
</div>

<!-- Load Code Modal -->
<div class="modal fade" id="loadCode" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Load Code to Members</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="/member/bco/send-code" method="POST">
        @csrf
        <div class="modal-body">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Search for Member</label>
                        <input type="text" id="autocomplete" class="form-control" placeholder="Search..." autocomplete="off" required>
                        <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Member User ID</label>
                        <input type="number" id="selected-id" name="loaded_to" class="form-control" readonly>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Select Package</label>
                        <select name="package" id="package" class="form-select" required>
                            <option value="" disabled selected></option>
                            <option value="silver">Silver</option>
                            <option value="gold">Gold</option>
                            <option value="diamond">Diamond</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Qty</label>
                        <input type="number" class="form-control" min="0" step="1" name="qty" id="qty" required> 
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Generate Code</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- resources/views/your-view.blade.php -->
<script type="text/javascript">
    var route = "{{ route('bco-autocomplete') }}";
    $('#autocomplete').typeahead({
        source: function (query, process) {
            return $.get(route, {
                query: query
            }, function (data) {
                return process(data);
            });
        },
        updater: function (item) {
            var selectedUser = item;
            $('#selected-id').val(selectedUser.id);

            return selectedUser.name;
    }
    });
</script>

@if(session('error'))
  <script>
    Swal.fire({
            icon: "error",
            title: "Error!",
            text: "{{ session('error') }}"
          });
  </script>
  @elseif(session('success'))
  <script>
    Swal.fire({
            icon: "success",
            title: "Success!",
            text: "{{ session('success') }}"
          });
  </script>
  @endif

<script src="{{asset('js/script.js')}}"></script>
@endsection()