@extends('base')

@section('contents')
<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded p-4">
        <h2>{{$referrals->count()}} Total Directs</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Address</th>
                    <th>Package</th>
                    <td>Date Encoded</td>
                </thead>
                <tbody>
                    @foreach($referrals as $key=>$referral)
                    <tr>
                        <td>{{@$key+1}}</td>
                        <td>{{@$referral->name}}</td>
                        <td>{{@$referral->mobile}}</td>
                        <td>{{@$referral->address}}</td>
                        <td>{{@$referral->package}}</td>
                        <td>{{@$referral->created_at->format('m-d-y')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $referrals->links() }}
        </div>
    </div>
</div>
@endsection