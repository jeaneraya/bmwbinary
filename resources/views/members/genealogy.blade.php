@extends('base')

@section('contents')

<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded p-4">
        <h2>Genealogy</h2>
        <div class="row g-4">
            <div class="col-12 mt-4">
                <div class="table-responsive">
                    <table class="table text-center genealogy-table" align="center">
                        <tr>
                            <?php
                                $treeController = app(\App\Http\Controllers\TreeController::class);
                                $treeData = $treeController->treeData($current_user);
                                $userData = $treeController->getTreeData($current_user);
                            ?>
                            <td colspan="2" style="width: 20%">
                                <p class="m-0">{{@$treeData->left_count}}</p>
                                <p class="m-0">{{@$treeData->left_points}} pts.</p>
                            </td>
                            <td colspan="2">
                                <p class="m-0">
                                    <iconify-icon icon="majesticons:user" style="font-size: 3em"
                                        class="{{ @$userData->package == 'silver' ? 'text-success' : (@$userData->package == 'gold' ? 'text-warning' : 'text-primary') }}">
                                    </iconify-icon>
                                </p>
                                <p class="m-0">{{@$userData->username}}</p>
                            </td>
                            <td colspan="2" style="width: 20%">
                                <p class="m-0">{{@$treeData->right_count}}</p>
                                <p class="m-0">{{@$treeData->right_points}} pts.</p>
                            </td>
                        </tr>
                        <tr>
                            <?php
                            $treeController = app(\App\Http\Controllers\TreeController::class);
                            $first_left_user = @$treeData->left;
                            $first_right_user = @$treeData->right;
                            $fleft_tree = $treeController->treeData(@$first_left_user);
                            $fright_tree = $treeController->treeData(@$first_right_user);
                            $fleft_info = $treeController->getTreeData(@$first_left_user);
                            $fright_info = $treeController->getTreeData(@$first_right_user);
                            ?>
                            <td colspan="3">
                                @if($first_left_user == 0)
                                <div class="dropdown font-sans-serif mb-2">
                                    <a class="dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-user-plus" style="font-size:3em; color:#5e6e82;"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="/member/form?sponsor={{auth()->user()->id}}&side=left&placement={{@$current_user}}">Add New Member</a>
                                        <a class="dropdown-item" href="/member/add-account?sponsor={{auth()->user()->id}}&side=left&placement={{@$current_user}}">Add Personal Account</a>
                                    </div>
                                </div>
                                @else
                                <a href="{{route('genealogy', ['id' => $first_left_user])}}"><i class="fas fa-user {{ @$fleft_info->package == 'silver' ? 'text-success' : (@$fleft_info->package == 'gold' ? 'text-warning' : 'text-primary') }}" style="font-size:3em; color:limegreen;"></i></a>
                                <p class="m-0">{{@$fleft_info->username}}</p>
                                <p class="m-0">{{@$fleft_info->account}}</p>
                                @endif
                            </td>
                            <td colspan="3">
                                @if($first_right_user == 0)
                                <div class="dropdown font-sans-serif mb-2">
                                    <a class="dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-user-plus" style="font-size:3em; color:#5e6e82;"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="/member/form?sponsor={{auth()->user()->id}}&side=right&placement={{@$current_user}}">Add New Member</a>
                                        <a class="dropdown-item" href="/member/add-account?sponsor={{auth()->user()->id}}&side=right&placement={{@$current_user}}">Add Personal Account</a>
                                    </div>
                                </div>
                                @else
                                <a href="{{route('genealogy', ['id' => $first_right_user])}}"><i class="fas fa-user {{ @$fright_info->package == 'silver' ? 'text-success' : (@$fright_info->package == 'gold' ? 'text-warning' : 'text-primary') }}" style="font-size:3em; color:limegreen;"></i></a>
                                <p class="m-0">{{@$fright_info->username}}</p>
                                <p class="m-0">{{@$fright_info->account}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            @php
                            $tl_first_left_info = $treeController->getTreeData(@$fleft_tree->left);
                            $tl_first_right_info = $treeController->getTreeData(@$fleft_tree->right);
                            $tl_second_left_info = $treeController->getTreeData(@$fright_tree->left);
                            $tl_second_right_info = $treeController->getTreeData(@$fright_tree->right);
                            @endphp
                            <td>
                                @if(@$fleft_tree->left == 0)
                                <div class="dropdown font-sans-serif mb-2">
                                    <a class="dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-user-plus" style="font-size:3em; color:#5e6e82;"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="/member/form?sponsor={{auth()->user()->id}}&side=left&placement={{@$first_left_user}}">Add New Member</a>
                                        <a class="dropdown-item" href="/member/add-account?sponsor={{auth()->user()->id}}&side=left&placement={{@$first_left_user}}">Add Personal Account</a>
                                    </div>
                                </div>
                                @else
                                <a href="{{route('genealogy', ['id' => @$fleft_tree->left])}}"><i class="fas fa-user {{ @$tl_first_left_info->package == 'silver' ? 'text-success' : (@$tl_first_left_info->package == 'gold' ? 'text-warning' : 'text-primary') }}" style="font-size:3em; color:limegreen;"></i></a>
                                <p class="m-0">{{@$tl_first_left_info->username}}</p>
                                <p class="m-0">{{@$tl_first_left_info->account}}</p>
                                @endif
                            </td>
                            <td colspan="2">
                                @if(@$fleft_tree->right == 0)
                                <div class="dropdown font-sans-serif mb-2">
                                    <a class="dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-user-plus" style="font-size:3em; color:#5e6e82;"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="/member/form?sponsor={{auth()->user()->id}}&side=right&placement={{@$first_left_user}}">Add New Member</a>
                                        <a class="dropdown-item" href="/member/add-account?sponsor={{auth()->user()->id}}&side=right&placement={{@$first_left_user}}">Add Personal Account</a>
                                    </div>
                                </div>
                                @else
                                <a href="{{route('genealogy', ['id' => @$tl_first_right_info->id])}}"><i class="fas fa-user {{ @$tl_first_right_info->package == 'silver' ? 'text-success' : (@$tl_first_right_info->package == 'gold' ? 'text-warning' : 'text-primary') }}" style="font-size:3em; color:limegreen;"></i></a>
                                <p class="m-0">{{@$tl_first_right_info->username}}</p>
                                <p class="m-0">{{@$tl_first_right_info->account}}</p>
                                @endif
                            </td>
                            <td>
                                @if(@$fright_tree->left == 0)
                                <div class="dropdown font-sans-serif mb-2">
                                    <a class="dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-user-plus" style="font-size:3em; color:#5e6e82;"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="/member/form?sponsor={{auth()->user()->id}}&side=left&placement={{@$first_right_user}}">Add New Member</a>
                                        <a class="dropdown-item" href="/member/add-account?sponsor={{auth()->user()->id}}&side=left&placement={{@$first_right_user}}">Add Personal Account</a>
                                    </div>
                                </div>
                                @else
                                <a href="{{route('genealogy', ['id' => $tl_second_left_info->id])}}"><i class="fas fa-user {{ @$tl_second_left_info->package == 'silver' ? 'text-success' : (@$tl_second_left_info->package == 'gold' ? 'text-warning' : 'text-primary') }}" style="font-size:3em; color:limegreen;"></i></a>
                                <p class="m-0">{{@$tl_second_left_info->username}}</p>
                                <p class="m-0">{{@$tl_second_left_info->account}}</p>
                                @endif
                            </td>
                            <td colspan="2">
                                @if(@$fright_tree->right == 0)
                                <div class="dropdown font-sans-serif mb-2">
                                    <a class="dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-user-plus" style="font-size:3em; color:#5e6e82;"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="/member/form?sponsor={{auth()->user()->id}}&side=right&placement={{@$first_right_user}}">Add New Member</a>
                                        <a class="dropdown-item" href="/member/add-account?sponsor={{auth()->user()->id}}&side=right&placement={{@$first_right_user}}">Add Personal Account</a>
                                    </div>
                                </div>
                                @else
                                <a href="{{route('genealogy', ['id' => $tl_second_right_info->id])}}"><i class="fas fa-user {{ @$tl_second_right_info->package == 'silver' ? 'text-success' : (@$tl_second_right_info->package == 'gold' ? 'text-warning' : 'text-primary') }}" style="font-size:3em; color:limegreen;"></i></a>
                                <p class="m-0">{{@$tl_second_right_info->username}}</p>
                                <p class="m-0">{{@$tl_second_right_info->account}}</p>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection