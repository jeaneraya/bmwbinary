@extends('base')

@section('contents')
<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded p-4">
        <div class="row g-4">
            <div class="col-sm-12 col-lg-8">
                <div class="row">
                    <div class="col-sm-12 col-lg-5">
                        <h2>Income Wallet: <span id="income-route" data-route="income-wallet"></span></h2>
                    </div> 
                    @if(auth()->user()->username == 'bmwbeda')
                    <div class="col-sm-12 col-lg-5">
                        <select name="wallet_account" id="wallet_account" class="form-select">
                            @foreach($accounts as $account)
                            <option value="{{$account->id}}">{{$account->account}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                </div>
                
                <div class="row g-4 mt-4">
                    <div class="col-sm-12 col-lg-6 mb-3">
                        <strong>Tree Points:</strong>
                        <p class="m-0 text-success fw-bold">Left: {{$tree->left_points}}</p>
                        <p class="m-0 text-primary fw-bold">Right: {{$tree->right_points}}</p>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <form action="{{route('add-to-encash', ['id' => $user_id, 'amount' => @$income->pairing, 'transaction' => 'encash-pairing'])}}">
                            <strong>Dream Points:</strong>
                            <p class="m-0 text-secondary">{{number_format(@$income->dream_points,2)}}</p>
                            <button class="btn btn-primary btn-sm mt-3"  
                            {{ @$income->dream_points < 10000 ? 'disabled' : '' }}>Convert Dream Points</button>
                        </form>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <form action="{{route('add-to-encash', ['id' => $user_id, 'amount' => @$income->pairing, 'transaction' => 'encash-pairing'])}}">
                            <strong>Total Pairing:</strong>
                            <p class="m-0 text-secondary">&#8369; {{number_format(@$income->pairing,2)}}</p>
                            <button class="btn btn-primary btn-sm mt-3 encash-pairing" 
                            data-pairing-amount="{{@$income->pairing}}" 
                            data-transaction="pairing" 
                            data-id="{{$user_id}}" 
                            {{ @$income->pairing == 0 ? 'disabled' : '' }}>Add to Encash Wallet</button>
                        </form>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <form action="{{route('add-to-encash', ['id' => $user_id, 'amount' => @$income->direct_referral, 'transaction' => 'direct-referral'])}}">
                            <strong>Total Direct Referral:</strong>
                            <p class="m-0 text-secondary">&#8369; {{number_format(@$income->direct_referral,2)}}</p>
                            <button class="btn btn-primary btn-sm mt-3 encash-direct-referral" 
                            data-direct-referrals="{{@$income->direct_referral}}" 
                            data-transaction="direct-referral" 
                            data-id="{{$user_id}}"
                            {{ @$income->direct_referral == 0 ? 'disabled' : '' }}>Add to Encash Wallet</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <h2>Encashment Method</h2>
                <form action="/member/encash-balance" method="POST">
                @csrf
                <div class="mt-4 mb-2">
                    <p class="m-0">Total Encashment:</p>
                    <p class="text-success fw-bold">&#8369; {{number_format(@$wallet_balance,2)}}</p>
                    <input type="hidden" name="amount" value="{{@$wallet_balance}}">
                    <input type="hidden" name="user_id" value="{{$user_id}}">
                    <select name="encash_method" id="encash_method" class="form-select my-3" required> 
                        <option value="" selected disabled>- Encashment Method -</option>
                        <option value="gcash">GCash</option>
                        <option value="bank">Bank </option>
                        <option value="otc">Over the CounTransferter</option>
                        <option value="ctc">Convert to Code</option>
                    </select>
                    <input type="text" name="encashment_details" id="encash_details" class="form-control mb-3 d-none" placeholder="Enter Account/GCash Number">
                    <div>
                        <div class="deductions mb-2">
                            <span>Less Tax:</span>
                            <span>10%</span>
                        </div>
                        <div class="deductions">
                            <span>Processing Fee:</span>
                            <span>&#8369; 25.00</span>
                        </div>
                    </div>
                </div>
                    <button class="btn btn-primary w-100" type="submit" {{ @$wallet_balance == 0 ? 'disabled' : '' }}>Encash Balance</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid pt-4 px-4">
<div class="bg-light rounded p-4">
        <h2 class="mb-4">Encashment History</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Account</th>
                    <th>Amount</th>
                    <th>Deduction</th>
                    <th>Total Encashment</th>
                    <th>MOP</th>
                    <th>Detail</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    @foreach($encashments as $key=>$encashment)
                    <tr>
                        <td>{{@$key+1}}</td>
                        <td>{{@$encashment->account}}</td>
                        <td>&#8369; {{number_format(@$encashment->amount,2)}}</td>
                        <td>&#8369; {{number_format(@$encashment->deduction,2)}}</td>
                        <td>&#8369; {{number_format(@$encashment->total_amount,2)}}</td>
                        <td>{{@$encashment->details}}</td>
                        <td>{{@$encashment->encashment_details}}</td>
                        <td>{{@$encashment->status}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $encashments->links() }}
        </div>
    </div>
</div>

<script src="{{asset('js/income-wallet.js')}}"></script>
@endsection