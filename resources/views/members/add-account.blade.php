@extends('base')

@section('contents')
<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded p-4">
        <div class="row g-4">
            <div class="col-sm-12 col-lg-8">
                <h2>Add New Account</h2>
                <form action="/member/registration" method="POST">
                @csrf 
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Sponsor</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{auth()->user()->name}}" readonly>
                            <input type="hidden" name="transaction" value="add-account">
                            <input type="hidden" name="email" value="{{$user->email}}">
                            <input type="hidden" name="username" value="{{$user->username}}">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Placement</label>
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" id="placement" name="placement" value="{{$placement}}">
                            <input type="text" class="form-control" value="{{$placement_name}}" readonly>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Side</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="side" value="{{$side}}" readonly>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Account</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="account" value="{{$account}}" readonly>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="" class="col-sm-2 col-form-label">Select Pin</label>
                        <div class="col-sm-10">
                            <select name="pins" id="pins" class="form-select">
                                <option value="" disabled selected></option>
                                @foreach ($pins as $pin)
                                <option value="{{@$pin->id}}">{{@$pin->pin_key}} {{@$pin->pin_code}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Add New Account</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection