<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Income;

class CronController extends Controller
{
    public function updateCron()
    {
        Income::where('pair_cycle', '!=', 0)->update(['pair_cycle' => 0]);
    }
}
