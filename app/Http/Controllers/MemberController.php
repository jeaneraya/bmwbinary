<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\PinList;
use App\Models\History;
use App\Models\Tree;
use App\Models\WalletBalance;
use App\Models\DirectReferral;
use App\Models\Income;
use App\Models\Encashment;
use App\Models\PinRequest;
use App\Models\BCOLoadHistory;
use Auth;

class MemberController extends Controller
{
    public function index() {
        $total_income = Income::where('user_id',auth()->user()->id)->selectRaw('SUM(total_pairing + total_referral) as total')->first()->total ?? 0;
        $total_pairing = Income::where('user_id',auth()->user()->id)->first()->total_pairing ?? 0;
        $total_referral = Income::where('user_id',auth()->user()->id)->first()->total_referral ?? 0;
        $total_encashments = Encashment::where('user_id',auth()->user()->id)->sum('amount');
        $income = Income::where('user_id',auth()->user()->id)->first();
        $histories = History::where('user_id',auth()->user()->id)->orderBy('id','desc')->paginate(15);

        return view('members.index')->with([
            'total_income' => $total_income,
            'total_pairing' => $total_pairing,
            'total_referral' => $total_referral,
            'total_encashments' => $total_encashments,
            'income' => $income,
            'histories' => $histories
        ]);
    }

    public function form(Request $request) {
        $data = $request->all();
    
        $user = User::where('username', auth()->user()->username)->first();
    
        if ($user) {
            $pins = PinList::where('user_id', $user->id)->where('status', 'available')->where('load_to','member')->get();
    
            return view('members.form')->with([
                'pins' => $pins,
                'datas' => $data,
                'placement' => $this->getSponsorName($data['placement'])
            ]);
        } 
    }
    

    public function authLogin(Request $request) {
        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            return redirect('/');
        } else {
            return redirect()->back()->with('error', 'Invalid Login Credentials')->withInput();
        }
    }

    public function memberRegistration(Request $request) {
        $data = $request->all();
        $transaction = $request->transaction;
        $checkEmail = true;
        $countAccount = $this->countUsername($data['username']);

        if($data['transaction'] == 'registration') {
            $data['name'] = $data['first_name']." ".$data['last_name'];
            unset($data['first_name']);
            unset($data['last_name']);
            
            $checkEmail = $this->checkEmail($data['email'], $data['username']);
        } 

        $checkPin = $this->checkPin($data['pins']);
        $package = $this->checkPinPackage($data['pins']);
        $sponsor = auth()->user()->id;
        $data['package'] = $package;
        $data['account'] = "Account 1";
        unset($data['transaction']);
        // dd($data);
        if($checkPin) {
            if($checkEmail) {
                if($countAccount < 32) {
                    if($request->transaction == 'add-account'){
                        
                        $user_details = User::where('id',$sponsor)->first();
                        $user_count = User::where('username',$user_details->username)->count();
                        $data['birthdate'] = $user_details->birthdate;
                        $data['address'] = $user_details->address;
                        $data['mobile'] = $user_details->mobile;
                        $data['password'] = $user_details->password;
                        $data['name'] = $user_details->name;
                        $data['email'] = $user_details->email;
                        $data['account'] = "Account " . $user_count + 1;

                        $emailParts = explode('@', $data['email']);
                        $emailParts[0] .= $user_count + 1;
                        $data['email'] = implode('@', $emailParts);

                    } 

                    $newAccount = User::create($data)->assignRole('member');
                    $new_account_id = $newAccount->id;
                    
                    $transaction_referral = "New Direct Referral to from " . $data['name'];

                    if($package == 'silver') {
                        $referral_fee = 300;
                        $points = 4;
                        $dream_points = 400;
                    } elseif($package == 'gold') {
                        $referral_fee = 600;
                        $points = 8;
                        $dream_points = 800;
                    } else {
                        $referral_fee = 1500;
                        $points = 20;
                        $dream_points = 2000;
                    }

                    History::create(['transactions' => $transaction_referral, 'user_id' => auth()->user()->id,'amount_points' => $referral_fee]);
                    DirectReferral::create(['downline' => $new_account_id, 'sponsor' => auth()->user()->id]);
                    Tree::create(['user_id' => $new_account_id]);
                    WalletBalance::create(['user_id' => $new_account_id]);
                    Tree::where('user_id', $data['placement'])->update([$data['side'] => $new_account_id]);
                    Income::where('user_id', $sponsor)->update([
                        'direct_referral' => \DB::raw('direct_referral + ' . $referral_fee),
                        'total_referral' => \DB::raw('total_referral + ' . $referral_fee),
                    ]);
                    Income::create(['user_id' => $new_account_id]);

                    $temp_sponsor = $data['placement'];
                    $temp_side = $data['side'];
                    $temp_side_count = $data['side'].'_count';
                    $temp_side_points = $data['side'].'_points';


                    $total_count = 1;
                    $i = 1;
                    $gets = [];

                    while($total_count > 0) {

                        $sponsorResult = Tree::where('user_id', $temp_sponsor)->first();
                        if (!is_null($sponsorResult)) {
                            $current_temp_side_count = isset($sponsorResult->$temp_side_count) ? $sponsorResult->$temp_side_count + 1 : 1;
                            $current_temp_side_points = isset($sponsorResult->$temp_side_points) ? $sponsorResult->$temp_side_points + $points : $points;
                            Tree::where('user_id', $temp_sponsor)->update([$temp_side_count => $current_temp_side_count, $temp_side_points => $current_temp_side_points]);
                        }

                        if($temp_sponsor !== null) {
                            $income_data = $this->income($temp_sponsor);

                            $tree_data = $this->tree($temp_sponsor);

                            $temp_left_count = $tree_data['left_count'];
                            $temp_right_count = $tree_data['right_count'];
                            $temp_left_points = $tree_data['left_points'];
                            $temp_right_points = $tree_data['right_points'];

                            if ($this->getAccountPackage($temp_sponsor) == 'silver') {
                                $limitIncome = 3200;
                            } elseif ($this->getAccountPackage($temp_sponsor) == 'gold') {
                                $limitIncome = 6400;
                            } else {
                                $limitIncome = 16000;
                            }

                            if($i < 7)
                            {
                                if($temp_side == "left")
                                {
                                    if($temp_left_points <= $temp_right_points && $temp_left_points > 0)
                                    {
                                        
                                        Income::where('user_id', $temp_sponsor)->update(['pair_cycle' => $income_data['pair_cycle'] + 1]);
                                        
                                        if($income_data['day_bal'] < $limitIncome && $income_data['pair_cycle'] !== 4 && $income_data['pair_cycle'] !== 9) 
                                        {
                                            $pairing_amount = $points * 100;
                                            $trans_pairing = "New Pairing Bonus from Member " . $data['username'];
                                            $new_pairing_balance = $income_data['pairing'] + $pairing_amount;
                                            $new_day_balance = $income_data['day_bal'] + $pairing_amount;
                                            $new_current_balance = $income_data['current_bal'] + $pairing_amount;
                                            $new_total_income = $income_data['total_pairing'] + $pairing_amount;
                                            $new_pair_cycle = $income_data['pair_cycle'] + 1;
                                            $points_data = [
                                                'right_points' => $temp_right_points - $points,
                                                'left_points' => $temp_left_points - $points
                                            ];
    
                                            History::create(['transactions' => $trans_pairing, 'user_id' => $temp_sponsor, 'amount_points' => $pairing_amount]);
                                            Tree::where('user_id',$temp_sponsor)->update($points_data);
                                            $pairing_data = [
                                                'pairing'       => $new_pairing_balance,
                                                'day_bal'       => $new_day_balance,
                                                'current_bal'   => $new_current_balance,
                                                'total_pairing' => $new_total_income
                                            ];
    
                                            Income::where('user_id', $temp_sponsor)->update($pairing_data);
                                        } elseif($income_data['pair_cycle'] == 4 || $income_data['pair_cycle'] == 9)
                                        {
                                            $trans_dream_points = "New Added Dream Points from Member ".$data['username'];
                                            History::create(['transactions' => $trans_dream_points, 'user_id' => $temp_sponsor,'amount_points' => $dream_points]);
                                            Income::where('user_id', $temp_sponsor)->update(['dream_points' => $income_data['dream_points'] + $dream_points]);
                                            $points_data = [
                                                'right_points' => $temp_right_points - $points,
                                                'left_points' => $temp_left_points - $points
                                            ];
                                            Tree::where('user_id',$temp_sponsor)->update($points_data);
                                        } else
                                        {
                                            Tree::where('user_id', $temp_sponsor)->update(['right_points' => max(0,$temp_right_points - $points), 'left_points' => max(0,$temp_left_points - $points)]);
                                        }
                                    } 
                                    elseif ($temp_right_points < $temp_left_points && $temp_right_points > 0) 
                                    {
                                        Income::where('user_id', $temp_sponsor)->update(['pair_cycle' => $income_data['pair_cycle'] + 1]);
                                        if($temp_right_points < 20 && $temp_right_points > 4) {
                                            $points = 8;
                                        } elseif($temp_right_points <= 4) {
                                            $points = 4;
                                        } 
                                        if($income_data['day_bal'] < $limitIncome && ($income_data['pair_cycle'] % 5) !== 4)
                                        {
                                            $pairing_amount = $points * 100;
                                            $trans_pairing = "New Pairing Bonus from Member " . $data['username'];
                                            $new_pairing_balance = $income_data['pairing'] + $pairing_amount;
                                            $new_day_balance = $income_data['day_bal'] + $pairing_amount;
                                            $new_current_balance = $income_data['current_bal'] + $pairing_amount;
                                            $new_total_income = $income_data['total_pairing'] + $pairing_amount;
                                            $new_pair_cycle = $income_data['pair_cycle'] + 1;
                                            $points_data = [
                                                'right_points' => max(0,$temp_right_points - $points),
                                                'left_points' => max(0,$temp_left_points - $points)
                                            ];
                                            History::create(['transactions' => $trans_pairing, 'user_id' => $temp_sponsor, 'amount_points' => $pairing_amount]);
                                            Tree::where('user_id',$temp_sponsor)->update($points_data);
                                            $pairing_data = [
                                                'pairing'       => $new_pairing_balance,
                                                'day_bal'       => $new_day_balance,
                                                'current_bal'   => $new_current_balance,
                                                'total_pairing' => $new_total_income
                                            ];
    
                                            Income::where('user_id', $temp_sponsor)->update($pairing_data);
                                        } elseif(($income_data['pair_cycle'] % 5) == 4)
                                        {
                                            $trans_dream_points = "New Added Dream Points from Member ".$data['username'];
                                            History::create(['transactions' => $trans_dream_points, 'user_id' => $temp_sponsor,'amount_points' =>$dream_points]);
                                            Income::where('user_id', $temp_sponsor)->update(['dream_points' => $income_data['dream_points'] + $dream_points]);
                                            $points_data = [
                                                'right_points' => max(0,$temp_right_points - $points),
                                                'left_points' => max(0,$temp_left_points - $points)
                                            ];
                                            Tree::where('user_id',$temp_sponsor)->update($points_data);
                                        } elseif($income_data['day_bal'] == $limitIncome)
                                        {
                                            Tree::where('user_id', $temp_sponsor)->update(['left_points' => max(0,$temp_left_points - $points),'right_points' => max(0,$temp_right_points - $points)]);
                                        }
                                    }
                                } elseif($temp_side == "right") 
                                {
                                    if($temp_right_points <= $temp_left_points && $temp_right_points > 0)
                                    {
                                        Income::where('user_id', $temp_sponsor)->update(['pair_cycle' => $income_data['pair_cycle'] + 1]);
    
                                        if($income_data['day_bal'] < $limitIncome && ($income_data['pair_cycle'] % 5) !== 4)
                                        {
                                            $pairing_amount = $points * 100;
                                            $trans_pairing = "New Pairing Bonus from Member " . $data['username'];
                                            $new_pairing_balance = $income_data['pairing'] + $pairing_amount;
                                            $new_day_balance = $income_data['day_bal'] + $pairing_amount;
                                            $new_current_balance = $income_data['current_bal'] + $pairing_amount;
                                            $new_total_income = $income_data['total_pairing'] + $pairing_amount;
                                            $new_pair_cycle = $income_data['pair_cycle'] + 1;
                                            $points_data = [
                                                'right_points' => max(0,$temp_right_points - $points),
                                                'left_points' => max(0,$temp_left_points - $points)
                                            ];
                                            History::create(['transactions' => $trans_pairing, 'user_id' => $temp_sponsor, 'amount_points' => $pairing_amount]);
                                            Tree::where('user_id',$temp_sponsor)->update($points_data);
                                            $pairing_data = [
                                                'pairing'       => $new_pairing_balance,
                                                'day_bal'       => $new_day_balance,
                                                'current_bal'   => $new_current_balance,
                                                'total_pairing' => $new_total_income
                                            ];
    
                                            Income::where('user_id', $temp_sponsor)->update($pairing_data);
                                        } elseif(($income_data['pair_cycle'] % 5) == 4)
                                        {
                                            $trans_dream_points = "New Added Dream Points from Member ".$data['username'];
                                            History::create(['transactions' => $trans_dream_points, 'user_id' => $temp_sponsor,'amount_points' =>$dream_points]);
                                            Income::where('user_id', $temp_sponsor)->update(['dream_points' => $income_data['dream_points'] + $dream_points]);
                                            $points_data = [
                                                'right_points' => max(0,$temp_right_points - $points),
                                                'left_points' => max(0,$temp_left_points - $points)
                                            ];
                                            Tree::where('user_id',$temp_sponsor)->update($points_data);
                                        } elseif($income_data['day_bal'] == $limitIncome)
                                        {
                                            Tree::where('user_id', $temp_sponsor)->update(['left_points' => max(0,$temp_left_points - $points),'right_points' => max(0,$temp_right_points - $points)]);
                                        }
                                    } elseif ($temp_left_points < $temp_right_points && $temp_left_points > 0) 
                                    {
                                        Income::where('user_id', $temp_sponsor)->update(['pair_cycle' => $income_data['pair_cycle'] + 1]);
                                        if($temp_left_points < 20 && $temp_left_points > 4) {
                                            $points = 8;
                                        } elseif($temp_left_points <= 4) {
                                            $points = 4;
                                        } 
                                        if($income_data['day_bal'] < $limitIncome && $income_data['pair_cycle'] !== 4 && $income_data['pair_cycle'] !== 9)
                                        {
                                            $pairing_amount = $points * 100;
                                            $trans_pairing = "New Pairing Bonus from Member " . $data['username'];
                                            $new_pairing_balance = $income_data['pairing'] + $pairing_amount;
                                            $new_day_balance = $income_data['day_bal'] + $pairing_amount;
                                            $new_current_balance = $income_data['current_bal'] + $pairing_amount;
                                            $new_total_income = $income_data['total_pairing'] + $pairing_amount;
                                            $new_pair_cycle = $income_data['pair_cycle'] + 1;
                                            $points_data = [
                                                'right_points' => max(0,$temp_right_points - $points),
                                                'left_points' => max(0,$temp_left_points - $points)
                                            ];
                                            History::create(['transactions' => $trans_pairing, 'user_id' => $temp_sponsor, 'amount_points' => $pairing_amount]);
                                            Tree::where('user_id',$temp_sponsor)->update($points_data);
                                            $pairing_data = [
                                                'pairing'       => $new_pairing_balance,
                                                'day_bal'       => $new_day_balance,
                                                'current_bal'   => $new_current_balance,
                                                'total_pairing' => $new_total_income
                                            ];
    
                                            Income::where('user_id', $temp_sponsor)->update($pairing_data);
                                        } elseif(($income_data['pair_cycle'] % 5) == 4)
                                        {
                                            if($temp_left_points < 20 && $temp_left_points > 4) {
                                                $points = 8;
                                            } elseif($temp_left_points <= 4) {
                                                $points = 4;
                                            } 

                                            $trans_dream_points = "New Added Dream Points from Member ".$data['username'];
                                            History::create(['transactions' => $trans_dream_points, 'user_id' => $temp_sponsor,'amount_points' =>$dream_points]);
                                            Income::where('user_id', $temp_sponsor)->update(['dream_points' => $income_data['dream_points'] + $dream_points]);
                                            $points_data = [
                                                'right_points' => max(0,$temp_right_points - $points),
                                                'left_points' => max(0,$temp_left_points - $points)
                                            ];
                                            Tree::where('user_id',$temp_sponsor)->update($points_data);
                                        } elseif($income_data['day_bal'] == $limitIncome)
                                        {
                                            Tree::where('user_id', $temp_sponsor)->update(['right_points' => max(0,$temp_right_points - $points),'left_points' => max(0,$temp_left_points - $points)]);
                                        }
                                    }
                                }
                            }

                            $next_sponsor = $this->getSponsor($temp_sponsor);
                            $temp_side = $this->getMemberSide($temp_sponsor);
                            $temp_side_count = $temp_side.'_count';
                            $temp_side_points = $temp_side.'_points';
                            $temp_sponsor = $next_sponsor;
                            $gets[] = $temp_sponsor;

                            $i++;
                        } else {
                            $total_count = 0;
                        }
                        
                    }
                    PinList::where('id', $data['pins'])->update(['status' => 'used']);
                    return redirect()->to('/genealogy/'.auth()->user()->id);

                } else {
                    return redirect()->back()->with('failed','Account Limit has Reached!');
                }
            } else {
                return redirect()->back()->with('failed','Email or Username Already Taken.');
            }
        } else {
            return redirect()->back()->with('failed','Code Not Available.');
        }
    }

    public function directReferrals()
    {
        $referrals = User::where('sponsor',auth()->user()->id)->paginate(25);

        return view('members.direct-referrals')->with([
            'referrals' => $referrals
        ]);
    }

    public function incomeWallet($id)
    {

        $income = Income::where('user_id',$id)->first();
        $wallet_balance = WalletBalance::where('user_id',$id)->selectRaw('SUM(pairing + direct_referral) as total_wallet')->first()->total_wallet ?? 0;
        $encashments = Encashment::where('user_id', auth()->user()->id)->orderBy('id', 'desc')->paginate(5);
        $username = User::where('id',$id)->first()->username;
        $accounts = User::where('username',$username)->get();
        $tree = Tree::where('user_id',$id)->first();

        return view('members.income-wallet')->with([
            'income' => $income,
            'wallet_balance' => $wallet_balance,
            'encashments' => $encashments,
            'accounts' => $accounts,
            'user_id' => $id,
            'tree'  => $tree
        ]);
    }

    public function addToEncashWallet($id,$amount,$transaction) 
    {
        if($transaction == 'encash-pairing') {
            WalletBalance::where('user_id',$id)->increment('pairing', $amount);
            Income::where('user_id',$id)->update(['current_bal' => 0, 'pairing' => 0]);
        } else {
            WalletBalance::where('user_id',$id)->increment('direct_referral',$amount);
            Income::where('user_id',$id)->update(['direct_referral' => 0]);
        }
        
        return redirect()->back();

    }

    public function encashBalance(Request $request) {
        $amount = $request->amount;
        $method = $request->encash_method;
        $userid = $request->user_id;
        $account = User::where('id',$userid)->first()->account;

        $tax = $amount * (10/100);
        $deduction = $tax + 25;

        $data = [
            'user_id'       => auth()->user()->id,
            'amount'        => $amount,
            'deduction'     => $deduction,
            'total_amount'  => $amount - $deduction,
            'details'       => $method,
            'status'        => 'pending',
            'account'       => $account,
            'encashment_details' => $request->encashment_details
        ];

        Encashment::create($data);
        WalletBalance::where('user_id',$userid)->update(['pairing' => 0, 'direct_referral' => 0]);

        return redirect()->back();
    }

    public function addAccount(Request $request)
    {
        $placement = $request->placement;
        $placement_name = $this->getSponsorName($placement);
        $side = $request->side;
        $userid = auth()->user()->id;

        $user = User::where('id',$userid)->first();
        $num_account = $this->countUsername($user->username);
        
        $account = 'Account '. $num_account + 1;

        $user = User::where('username', $user->username)->first();
    
        if ($user) {
            $pins = PinList::where('user_id', $user->id)->where('status', 'available')->where('load_to','member')->get();
        } 
        return view('members.add-account')->with([
            'placement' => $placement,
            'placement_name'    => $placement_name,
            'side'  => $side,
            'account' => $account,
            'pins' => $pins,
            'user' => $user
        ]);
    }

    public function BCOLoadCode()
    {   

        $bco_load_histories = BCOLoadHistory::where('user_id',auth()->user()->id)->paginate(8);
        $available_code = PinList::selectRaw('LEFT(pin_key, 2) as prefix, COUNT(*) as count')
        ->where('user_id', auth()->user()->id)
        ->where('load_to', 'bco')
        ->where('status', 'available')
        ->groupBy('prefix')
        ->get();

        $gold = 0;
        $silver = 0;
        $diamond = 0;

        foreach($available_code as $code)
        {
            $code->prefix == 'AG' ? $silver = $code->count : 0;
            $code->prefix == 'AU' ? $gold = $code->count : 0;
            $code->prefix == 'DM' ? $diamond = $code->count : 0;
        }
    
        $purchased_code = PinRequest::where('user_id',auth()->user()->id)->where('load_to','bco')->sum('qty');

        return view('members.load-code')->with([
            'bco_load_histories' => $bco_load_histories,
            'gold'      => $gold,
            'silver'    => $silver,
            'diamond'   => $diamond,
            'purchased_code'    => $purchased_code
        ]);
    }

    public function sendCode(Request $request)
    {
        $data = $request->all();
        if($data['package'] == 'silver') {
            $data['amount'] = 2988 * $data['qty'];
        } elseif($data['package'] == 'gold') {
            $data['amount'] = 5988 * $data['qty'];
        } else {
            $data['amount'] = 14988 * $data['qty'];
        }

        $pins = PinList::where('user_id',auth()->user()->id)->where('package',$data['package'])->where('load_to','bco')->where('status','available')->orderBy('id','asc')->limit($data['qty']);
        $count_available_package = PinList::where('user_id',auth()->user()->id)->where('package',$data['package'])->where('load_to','bco')->count();
        BCOLoadHistory::create($data);

        if($count_available_package >= $data['qty']){
            $pins->update([
                'user_id'   => $data['loaded_to'],
                'load_to'   => 'member'
            ]);

            return redirect()->back()->with('success', 'Code/s sent successfully!');
        } else {
            return redirect()->back()->with('error', 'Not enough available code/s.');
        }

    }

    public function autocompleteBCOMembers(Request $request) {
        $query = $request->get('query');
        $filterResult = User::where('name', 'LIKE', '%' . $query . '%')
        ->orderBy('name')
        ->limit(1)
        ->get();
    
        return response()->json($filterResult);
    }

    public function loginAccount($id) {
        $user = User::where('id',$id)->first();

        if($user) {
            Auth::Login($user);
    
            return redirect("/");
        } 
    }

    function checkPin($pin) {
        $result = PinList::where('id', $pin)->first();
        return $result->status == 'available' ? true : false;
    }

    function checkEmail($email, $username) {
        $result = User::where('email', $email)->first();
        $result2 = User::where('username', $username)->first();

         return ($result || $result2) ? false : true;
    }

    function countAccount($email) {
        $result = User::where('email', $email)->count();
        return $result;
    }

    function getSponsorName($id) {
        $result = User::where('id', $id)->first()->name;
        return $result;
    }

    function checkPinPackage($id) {
        $result = PinList::where('id', $id)->first()->package;
        return $result;
    }

    function income($userid) {
        $data = [];
        $query = Income::where('user_id', $userid)->first();
        if ($query) {
            $data['pairing'] = $query['pairing'];
            $data['pair_cycle'] = $query['pair_cycle'];
            $data['day_bal'] = $query['day_bal'];
            $data['current_bal'] = $query['current_bal'];
            $data['total_pairing'] = $query['total_pairing'];
            $data['dream_points'] = $query['dream_points'];
        } else {
            $data['pairing'] = 0;
            $data['pair_cycle'] = 0;
            $data['day_bal'] = 0;
            $data['current_bal'] = 0;
            $data['total_pairing'] = 0;
            $data['dream_points'] = 0;
        }
    
        return $data;
    }

    function tree($userid) {
        $data = [];
        $query = Tree::where('user_id', $userid)->first();
        if($query) {
            $data['left'] = $query['left'];
            $data['right'] = $query['right'];
            $data['left_count'] = $query['left_count'];
            $data['right_count'] = $query['right_count'];
            $data['left_points'] = $query['left_points'];
            $data['right_points'] = $query['right_points'];
        } else {
            $data['left'] = 0;
            $data['right'] = 0;
            $data['left_count'] = 0;
            $data['right_count'] = 0;
            $data['left_points'] = 0;
            $data['right_points'] = 0;
        }
    
        return $data;
    }

    function getSponsor($binary_sponsor) {
        $user = User::where('id', $binary_sponsor)->first();
    
        return $user !== null ? $user->placement : null;
    }    

    function getMemberSide($binary_sponsor) {
        $result = User::where('id', $binary_sponsor)->first();
        return $result !== null ? $result->side : null;
    }

    function countUsername($username) {
        $result = User::where('username', $username)->count();
        return $result;
    }

    public function accounts($user) {
        $accounts = User::where('username',$user)->get();

        return $accounts;
    }

    public function getAccountPackage($userid) {
        $package = User::where('id',$userid)->first()->package;

        return $package;
    }

}
