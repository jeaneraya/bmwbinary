<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\PinRequest;
use App\Models\PinList;
use App\Models\Encashment;
use Auth;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    public function adminAuthLogin(Request $request) {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect('/admin');
        } else {
            return redirect()->back()->with('error', 'Invalid Login Credentials')->withInput();
        }
    }

    public function index() {
        return view('administrator.admin-dashboard');
    }
    
    public function autocompleteMembers(Request $request) {
        $query = $request->get('query');
        $filterResult = User::where('name', 'LIKE', '%' . $query . '%')
        ->orderBy('name')
        ->limit(1)
        ->get();
    
        return response()->json($filterResult);
    }

    public function loadCode() {
        $pin_requests = PinRequest::where('status', 'pending')->where('generated_by',auth()->user()->id)->get();

        return view('administrator.load-code')->with(['pin_requests' => $pin_requests]);
    }

    public function loadCodeToMember(Request $request) {
        $data = $request->all();

        if($data['package'] == 'silver') {
            $total = 2988 * $data['qty'];
        } elseif($data['package'] == 'gold') {
            $total = 5988 * $data['qty'];
        } else {
            $total = 14988 * $data['qty'];
        }

        $data['total'] = $total;
        $data['trans_num'] = $data['user_id'] . Str::random(6);
        $data['generated_by'] = auth()->user()->id;

        $user = User::where('id',$data['user_id'])->orderBy('id','asc')->first();
        if($user->account_type == 'bco')
        {
            $data['load_to'] = 'bco';
        }
        PinRequest::create($data);

        return redirect()->back();
    }

    public function generateCode($id) {
        $record = PinRequest::where('id', $id)->first();

        if($record->package == 'silver') {
            $initial = 'AG';
        } elseif($record->package == 'gold') {
            $initial = 'AU';
        } else {
            $initial = 'DM';
        }

        for($i = 0; $i < $record->qty; $i++) {
            $pin_key = $initial . Str::random(6);
            $pin_code = $record->user_id . str_pad(rand(10000, 99999), 6, '0', STR_PAD_LEFT);

            $data = [
                'user_id' => $record->user_id,
                'package' => $record->package,
                'pin_key' => $pin_key,
                'pin_code' => $pin_code,
                'loader'    => auth()->user()->id
            ];

            if($record->load_to == 'bco')
            {
                $data['load_to'] = 'bco';
            }

            PinList::create($data);
        }

        PinRequest::where('id', $id)->update(['status' => 'transferred']);

        return redirect()->back();
    }

    public function encashments()
    {
        $pendingEncashments = Encashment::where('status', 'pending')->orderBy('id','desc')->paginate(20, ['*'], 'pending_page');
        $releasedEncashments = Encashment::where('status', 'released')->orderBy('id','desc')->paginate(20, ['*'], 'released_page');

        return view('administrator.encashments', compact('pendingEncashments','releasedEncashments'));
    }

    public function approveEncashment($id)
    {
        Encashment::where('id',$id)->update(['status' => 'released']);
        return redirect()->back();
    }

    public function getMembersList()
    {
        $members = User::groupBy('username')->paginate(20);

        return view('administrator.members-list')->with(['members' => $members]);
    }

    public function upgradeMember($id)
    {

        $user = User::where('id',$id)->update(['account_type' => 'bco']);

        return redirect()->back();
    }

}
