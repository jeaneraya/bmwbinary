<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tree;
use App\Models\User;

class TreeController extends Controller
{
    public function genealogy($id) 
    {
        return view('members.genealogy')->with(['current_user' => $id]);
    }

    public function treeData($search) 
    {
        $tree_datas = Tree::where('user_id', $search)->first();
        return $tree_datas;
    }

    public function getTreeData($search) 
    {
        $tree_infos = User::where('id', $search)->first();
        return $tree_infos;
    }
}

