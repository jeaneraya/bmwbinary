<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PinRequest;
use App\Models\PinList;
use Illuminate\Support\Str;

class GeneralController extends Controller
{

    public function loadCode() {
        $pin_requests = PinRequest::where('generated_by',auth()->user()->id)->where('status', 'pending')->get();

        if(auth()->user()->hasRole('superadmin')){
            return view('administrator.load-code')->with(['pin_requests' => $pin_requests]);
        } else {
            return view('member.load-code')->with(['pin_requests' => $pin_requests]);
        }
    }

    public function loadCodeToMember(Request $request) {
        $data = $request->all();

        if($data['package'] == 'silver') {
            $total = 2988 * $data['qty'];
        } elseif($data['package'] == 'gold') {
            $total = 5988 * $data['qty'];
        } else {
            $total = 14988 * $data['qty'];
        }

        $data['total'] = $total;
        $data['trans_num'] = $data['user_id'] . Str::random(6);
        $data['generated_by'] = auth()->user()->id;

        PinRequest::create($data);

        return redirect()->back();
    }

    public function generateCode($id) {
        $record = PinRequest::where('id', $id)->first();

        if($record->package == 'silver') {
            $initial = 'AG';
        } elseif($record->package == 'gold') {
            $initial = 'AU';
        } else {
            $initial = 'DM';
        }

        for($i = 0; $i < $record->qty; $i++) {
            $pin_key = $initial . Str::random(6);
            $pin_code = $record->user_id . str_pad(rand(10000, 99999), 6, '0', STR_PAD_LEFT);

            $data = [
                'user_id' => $record->user_id,
                'package' => $record->package,
                'pin_key' => $pin_key,
                'pin_code' => $pin_code,
            ];

            PinList::create($data);
        }

        PinRequest::where('id', $id)->update(['status' => 'transferred']);

        return redirect()->back();
    }
}
