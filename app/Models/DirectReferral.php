<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DirectReferral extends Model
{
    use HasFactory;
    protected $guarded = [];
}
