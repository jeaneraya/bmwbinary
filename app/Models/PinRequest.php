<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PinRequest extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $appends = ['name'];

    public function users() {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function getNameAttribute()
    {
        return User::where('id', $this->user_id)->first()->name;
    }
}
