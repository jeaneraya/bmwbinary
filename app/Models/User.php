<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'user_details',
        'birthdate',
        'address',
        'mobile',
        'sponsor',
        'placement',
        'side',
        'pins',
        'username',
        'package',
        'account'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function getDataAttribute($value) {
        return json_decode($value);
    }

    public function direct_referrals() {
        return $this->hasMany(DirectReferral::class, 'user_id', 'id');
    }

    public function encashments()
    {
        return $this->hasMany(Encashment::class);
    }

    public function histories() {
        return $this->hasMany(History::class, 'user_id', 'id');
    }

    public function incomes() {
        return $this->hasMany(Income::class, 'user_id', 'id');
    }

    public function pin_lists()
    {
        return $this->hasMany(PinList::class, 'user_id');
    }

    public function pin_requests() {
        return $this->hasMany(PinRequest::class, 'user_id', 'id');
    }

    public function trees() {
        return $this->hasMany(Tree::class, 'user_id', 'id');
    }

    public function waller_balance() {
        return $this->hasMany(WalletBalance::class, 'user_id', 'id');
    }

    public function loaded_to_user()
    {
        return $this->hasMany(BCOLoadHistory::class, 'loaded_to');
    }
}
