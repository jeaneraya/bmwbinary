<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BCOLoadHistory extends Model
{
    use HasFactory;
    protected $table = 'bco_load_histories';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'loaded_to');
    }
}
