<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('incomes', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->double('pairing')->default(0);
            $table->double('direct_referral')->default(0);
            $table->double('current_bal')->default(0);
            $table->double('day_bal')->default(0);
            $table->double('total_pairing')->default(0);
            $table->double('total_referral')->default(0);
            $table->integer('pair_cycle')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('incomes');
    }
};
