<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pin_lists', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('package');
            $table->string('code_type')->nullable();
            $table->string('pin_key');
            $table->string('pin_code');
            $table->string('status')->default('available'); //used, available
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pin_lists');
    }
};
