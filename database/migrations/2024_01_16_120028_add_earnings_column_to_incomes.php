<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('incomes', function (Blueprint $table) {
            $table->double('dream_points')->default(0);
            $table->double('mutual_fund')->default(0);
            $table->double('profit_sharing')->default(0);
            $table->double('total_profit_sharing')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('incomes', function (Blueprint $table) {
            $table->dropColumn('dream_points');
            $table->dropColumn('mutual_fund');
            $table->dropColumn('profit_sharing');            
            $table->dropColumn('total_profit_sharing');
        });
    }
};
