<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('birthdate')->nullable();
            $table->string('address')->nullable();
            $table->integer('sponsor')->nullable();
            $table->integer('placement')->nullable();
            $table->string('side')->nullable();
            $table->integer('pins')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('birthdate');
            $table->dropColumn('address');
            $table->dropColumn('sponsor');
            $table->dropColumn('placement');
            $table->dropColumn('side');
            $table->dropColumn('pins');
        });
    }
};
