<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $member = Permission::updateOrCreate(['name' => 'member_access']);
        $superAdmin = Permission::updateOrCreate(['name' => 'system_config']);

        Role::updateOrCreate(['name' => 'member'])->givePermissionTo(['member_access']);
        Role::updateOrCreate(['name' => 'superadmin'])->givePermissionTo(['system_config']);

        $memberRole = Role::where('name', 'member')->first();
        $superadminRole = Role::where('name', 'superadmin')->first();

        $memberRole->givePermissionTo([$member]);
        $superadminRole->givePermissionTo([$superAdmin]);
    }
}
