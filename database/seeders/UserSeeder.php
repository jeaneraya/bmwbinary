<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::updateOrCreate(
            ['name' => 'superadmin'],
            ['email' => 'superadmin@bmwbeda.com',
            'password' => \Hash::make('bmwbeda@2023')
        ])->assignRole('superadmin');
        User::updateOrCreate(
            ['name' => 'bmwbeda'],
            [
            'email' => 'bmwbeda@email.com',
            'password' => \Hash::make('bmwbeda'),
            'username' => 'bmwbeda',
            'sponsor' => 1,
            'placement' => 1,
            'side' => 'left' 
        ])->assignRole('member');
    }
}
