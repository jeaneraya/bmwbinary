$(document).ready(function() {
    $('.encash-pairing').on('click', function() {
        $amount = $(this).data('pairing-amount');
    });

    var routeValue = window.location.pathname.split('/').pop();
    $('#wallet_account').val(routeValue);
    console.log(routeValue);

    $('#wallet_account').on('change', function(){

        var account = $(this).val();

        var redirectURL = "/member/income-wallet/" + account;
    
        window.location.href = redirectURL;
    });

    $('#encash_method').on('change', function() {
        var encash_method = $(this).val();

        if(encash_method == 'gcash' || encash_method == 'bank') {
            $('#encash_details').removeClass('d-none');
        } else {
            $('#encash_details').addClass('d-none');
        }
    });
    
});
