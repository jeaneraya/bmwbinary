<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\TreeController;
use App\Http\Controllers\CronController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(['middleware'=> ['auth']], function() {
    Route::get('/', [MemberController::class, 'index'])->middleware('can:member_access')->name('member-dashboard');
    
    Route::group(['prefix' => 'admin', 'middleware' => ['role:superadmin']], function (){
        Route::get('/',[AdminController::class, 'index'])->name('admin-dashboard');
        Route::get('/load-code',[AdminController::class, 'loadCode'])->name('load-code');
        Route::post('/load-code-to-member',[AdminController::class, 'loadCodeToMember']);
        Route::get('/generate-code/{id}',[AdminController::class, 'generateCode'])->name('generate-code');
        Route::get('/encashments',[AdminController::class, 'encashments'])->name('admin-encashments');
        Route::get('/approve-encashment/{id}',[AdminController::class, 'approveEncashment']);
        Route::get('/members-list',[AdminController::class, 'getMembersList']);
        Route::get('/upgrade-member/{id}',[AdminController::class, 'upgradeMember']);
    });
    
    Route::group(['prefix' => 'member', 'middleware' => ['role:member']], function (){
        Route::get('/form', [MemberController::class, 'form'])->name('add-member');
        Route::post('/registration', [MemberController::class, 'memberRegistration'])->name('member-registration');
        Route::get('/add-account',[MemberController::class, 'addAccount'])->name('add-account');
        Route::get('/direct-referrals',[MemberController::class,'directReferrals'])->name('direct-referrals');
        Route::get('/income-wallet/{id}',[MemberController::class, 'incomeWallet'])->name('income-wallet');
        Route::get('/income-wallet/{id?}/{amount?}/{transaction?}',[MemberController::class, 'addToEncashWallet'])->name('add-to-encash');
        Route::post('/encash-balance',[MemberController::class, 'encashBalance']);
        Route::get('/login/account/{id}',[MemberController::class, 'loginAccount']);
        Route::get('/bco/load-code',[MemberController::class, 'BCOLoadCode']);
        Route::post('/bco/send-code',[MemberController::class, 'sendCode']);
        Route::get('/bco/autocomplete',[MemberController::class, 'autocompleteBCOMembers'])->name('bco-autocomplete');
    });

    Route::get('/genealogy/{id}',[TreeController::class, 'genealogy'])->name('genealogy');
    Route::get('/autocomplete/search',[AdminController::class, 'autocompleteMembers'])->name('autocomplete-name');



    
    

    Route::get('/logout', function () {
        if(auth()->user()->hasRole('superadmin')) {
            auth()->logout();
            return redirect('/administrator/auth');
        } else {
            auth()->logout();
            return redirect('/member/login');
        }
    })->name('logout');
});

Route::get('/member/login', function() { return view('members.member-login'); })->name('login');
Route::post('/member/login/auth', [MemberController::class, 'authLogin'])->name('auth-login');

Route::get('/administrator/auth', function() { return view('administrator.admin-login'); })->name('admin-login');
Route::post('/administrator/authenticate', [AdminController::class, 'adminAuthLogin'])->name('admin-auth-login');

Route::get('/status/cron',[CronController::class,'updateCron']);



